{
    BSD 3-Clause License
    Copyright (c) 2021, Jerome Shidel
    All rights reserved.
}

{$I DANGER.DEF}
unit Intro;

interface

uses Danger;

procedure PlayIntro;

implementation

procedure PlayIntro;
var
    LTT : longint;
    FH, FX, I : integer;
    TP : word;
    P  : PAsset;
    BF : PFont;
    FS : TFontSettings;
    S : String;
    FC : TColor;
    E : TEvent;
    SP, FP : TRGBPalettes;
begin
    Video^.SetSync(True);
    PurgeEvents;
    GetFontSettings(FS);
    Video^.GetPalettes(SP);
    S := '1214N-' + Language;
    FontBestMatch(S, BF);
    if not AssetLoad('INTRO.' + Language, asDefault, P) then
        if not AssetLoad('INTRO.EN', asDefault, P) then exit;
    if P^.MemSize > 0 then begin
        TP := 0;
        FC := 7;
        LTT := TimerTick;
        E.Kind := evNull;
        repeat
            S := '';
            while TP < P^.MemSize - 1 do begin
                if (Bytes(P^.Data^)[TP] = 10) or (Bytes(P^.Data^)[TP] = 0) then begin
                   { Ignore }
                end else
                if (Bytes(P^.Data^)[TP] = 13) then begin
                    Break;
                end else
                    S := S + Chars(P^.Data^)[TP];
                Inc(TP);
            end;
            Inc(TP);
            if (TP > P^.MemSize - 5) then begin
                if Assigned(BF) then Video^.SetFont(BF);
                FC := 12;
            end;
            FH := Video^.TextHeight(S);
            FX := GetMaxX div 2 - Video^.TextWidth(S) div 2;
            for I := 0 to FH - 1 do begin
                Video^.Shift(dmUp, 1, 0);
                Video^.PutText(FX, GetMaxY - I, S, FC);
                While (LTT = TimerTick) do;
                LTT := TimerTick;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then break;
            end;
        until TP >= P^.MemSize;
        if (E.Kind <> evKeyPress) and (E.Kind <> evMouseClick) then
            for TP := 0 to GetMaxY div 2 do begin
                Video^.Shift(dmUp, 1, 0);
                While (LTT = TimerTick) do;
                LTT := TimerTick;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then break;
            end;
        if (E.Kind <> evKeyPress) and (E.Kind <> evMouseClick) then
            for TP := 0 to 33 do begin
                Video^.Shift(dmUp, 1, 0);
                FP := SP;
                Video^.FadePalettes(FP, TP * 3);
                Video^.SetPalettes(FP);
                While (LTT = TimerTick) do;
                LTT := TimerTick;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then break;
            end;
    end;
    Video^.Fill(0);
    Video^.Update;
    Video^.SetPalettes(SP);
    SetFontSettings(FS);
    AssetUnlock(P);
end;

end.